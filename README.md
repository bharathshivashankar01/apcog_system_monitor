# APCOG_System_Monitor

System Monitor Tool provides the user with data regarding different system parameters measured of a linux(system/server).

Few Parameters which System Monitoring tool measures are as follows:
•	CPU
•	Memory
•	Disk
•	Network
•	Sensors
•	Processes

1.CPU:

 Architecture: CPU Architecture.
 O/P Modes: CPU operating modes.
 Byte Order: Order in which the sequence of bytes is stored.
 Threads per Core: Number of threads for each core
 Core per Socket: Number of cores for each socket
 Sockets: Number of CPU sockets.
 Model Name: Processor name
 CPU MHz: Clock Speed

2. MEMORY:
 Total: Total System memory.
 Used: The amount of memory used.
 Free: The amount of memory free.
 Active, Inactive: A page cache optimizes access to files. These buffers are used for it


3. DISK:
 Name: Name of the partitioned drive.
 Size: Total memory space of disk.
 Used: Used Memory.
 Available: Available Memory.
 Mount Point: Directory where additional information is logically connected from storage location


4. NETWORK:
 Protocol: Protocol name.
 Received: The process’s owner.
 Sent: The percentage of the processor time used by the process.
 Local Address: Private IP address.
 Foreign Address: Host IP address.
 State: The name of the command that started the process.
 Program Name: The name of the program running.


5. SENSORS:
 Temperature: System Temperature.


6. PROCESSES:
 PID: Every time a new process is started it is assigned an identification number (ID) which is called process ID or PID for short.
 User: The process’s owner.
 CPU %: The percentage of the processor time used by the process.
 Memory %: The percentage of physical RAM used by the process.
 Time: How much processor time the process has used.
 Command: The name of the command that started the process.
