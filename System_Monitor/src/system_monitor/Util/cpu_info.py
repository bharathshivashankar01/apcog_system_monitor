import json
from logger import cpuInfo_logger
from System_Monitor.src.system_monitor.Model import Cpu


def sshConnectCPU_Info(host, username, password, json_Path):
    try:

        import paramiko

        # Connect to remote host
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(host, username=username, password=password)

        stdin, stdout, stderr = client.exec_command('lscpu')
        stdin_host, stdout_host, stderr_host = client.exec_command('hostname')
        output = stdout.readlines()
        for lines in output:
            cpuInfo_logger.info(lines)

        CPU_Info(output, json_Path)

        stdin.close()
        stdout.close()
        stderr.close()
        stderr_host.close()
        stdout_host.close()
        stdin_host.close()
        client.close()
    except IndexError:
        pass


def CPU_Info(output, json_Path):
    cpu = Cpu.Cpu()
    cpu.set_architecture = output[0].split()[1]

    cpu.set_op_modes = output[1].split()[2:]  # returns a list, convert this to string and assign to set_op_modes
    cpu.set_byte_order = output[2].split()[2:]  # returns a list, convert to string
    cpu.set_cpu_s = int(output[3].split()[1])  # proper
    cpu.set_online_cpu = str(output[4].split()[3])  # proper
    cpu.set_threads_per_core = int(output[5].split()[3])  # proper
    cpu.set_core_per_socket = int(output[6].split()[3])  # proper
    cpu.set_sockets = int(output[7].split()[1])  # proper
    cpu.set_numa_nodes = int(output[8].split()[2])  # proper
    cpu.set_vendor_id = output[9].split()[2]  # proper
    cpu.set_cpu_family = int(output[10].split()[2])  # proper
    cpu.set_model = int(output[11].split()[1])  # proper
    cpu.set_model_name = output[12].split()[2:]  # returns list. convert to list and save
    cpu.set_stepping = int(output[13].split()[1])  # proper
    cpu.set_cpu_mhz = float(output[14].split()[2])  # proper
    cpu.set_cpu_max_mhz = float(output[15].split()[3])  # proper
    cpu.set_cpu_min_mhz = float(output[16].split()[3])  # proper
    cpu.set_bogo_mips = float(output[17].split()[1])  # proper
    cpu.set_virualization_type = output[18].split()[1]  # proper
    cpu.set_l1d_cache = output[19].split()[2]  # proper
    cpu.set_l1i_cache = output[20].split()[2]  # proper
    cpu.set_l2_cache = output[21].split()[2]  # proper
    cpu.set_l3_cache = output[22].split()[2]  # proper

    cpu_json = json.dumps(cpu, default=lambda x: x.__dict__, sort_keys=False, indent=4)
    file = open(json_Path + "cpuInfo.json", "w")
    file.write(cpu_json)
    file.close()

    # print(cpu_json)

