import json

from System_Monitor.src.system_monitor.Model import SystemInformation


def sshConnectSysInfo(host, username, password ,json_Path):
    try:

        import paramiko

        # Connect to remote host
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(host, username=username, password=password)

        stdin_host, stdout_host, stderr_host = client.exec_command('hostname')
        hostName = stdout_host.readlines()[0]

        system_info(host, hostName,json_Path)

        print(hostName)

        stdin_host.close()
        stdout_host.close()
        stderr_host.close()
        stderr_host.close()
        stdout_host.close()
        stdin_host.close()
        client.close()

    except IndexError:

        pass

def system_info(host_ip, host_name,json_Path ):

    system_information = SystemInformation.Information()

    system_information.set_host_name = host_name
    system_information.set_ip_address = host_ip

    system_json = json.dumps(system_information, default=lambda x: x.__dict__, sort_keys=False, indent=4)
    file = open(json_Path + "temperature.json", "w")
    file.write(cpu_json)
    file.close()

    print(system_json)

