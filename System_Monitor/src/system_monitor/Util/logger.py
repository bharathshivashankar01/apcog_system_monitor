import logging

from logging import FileHandler
from logging import Formatter

LOG_FORMAT = ("%(asctime)s [%(levelname)s]: %(message)s in %(pathname)s:%(lineno)d")
log_Path = "/nfs/systemmonitor/"
LOG_LEVEL = logging.INFO

# pid logger
PID_LOG_FILE = log_Path + "pid.log"

pid_logger = logging.getLogger("System_Monitor.src.system_monitor.Util.pid")
pid_logger.setLevel(LOG_LEVEL)
pid_file_handler = FileHandler(PID_LOG_FILE)
pid_file_handler.setLevel(LOG_LEVEL)
pid_file_handler.setFormatter(Formatter(LOG_FORMAT))
pid_logger.addHandler(pid_file_handler)

# memory logger
MEMORY_LOG_FILE = log_Path + "memory.log"
memory_logger = logging.getLogger("System_Monitor.src.system_monitor.Util.memory")

memory_logger.setLevel(LOG_LEVEL)
memory_file_handler = FileHandler(MEMORY_LOG_FILE)
memory_file_handler.setLevel(LOG_LEVEL)
memory_file_handler.setFormatter(Formatter(LOG_FORMAT))
memory_logger.addHandler(memory_file_handler)

# sensor logger
SENSORS_LOG_FILE = log_Path + "sensors.log"

sensors_logger = logging.getLogger("System_Monitor.src.system_monitor.Util.sensors")
sensors_logger.setLevel(LOG_LEVEL)
sensors_file_handler = FileHandler(SENSORS_LOG_FILE)
sensors_file_handler.setLevel(LOG_LEVEL)
sensors_file_handler.setFormatter(Formatter(LOG_FORMAT))
sensors_logger.addHandler(sensors_file_handler)

# network logger
NETWORK_LOG_FILE = log_Path + "network.log"

network_logger = logging.getLogger("System_Monitor.src.system_monitor.Util.network")
network_logger.setLevel(LOG_LEVEL)
network_file_handler = FileHandler(NETWORK_LOG_FILE)
network_file_handler.setLevel(LOG_LEVEL)
network_file_handler.setFormatter(Formatter(LOG_FORMAT))
network_logger.addHandler(network_file_handler)

# cpuInfo logger
CPUINFO_LOG_FILE = log_Path + "cpuInfo.log"

cpuInfo_logger = logging.getLogger("System_Monitor.src.system_monitor.Util.cpu_info")
cpuInfo_logger.setLevel(LOG_LEVEL)
cpuInfo_file_handler = FileHandler(CPUINFO_LOG_FILE)
cpuInfo_file_handler.setLevel(LOG_LEVEL)
cpuInfo_file_handler.setFormatter(Formatter(LOG_FORMAT))
cpuInfo_logger.addHandler(cpuInfo_file_handler)

# disk logger
DISK_LOG_FILE = log_Path + "disk.log"

disk_logger = logging.getLogger("System_Monitor.src.system_monitor.Util.pid")
disk_logger.setLevel(LOG_LEVEL)
disk_file_handler = FileHandler(DISK_LOG_FILE)
disk_file_handler.setLevel(LOG_LEVEL)
disk_file_handler.setFormatter(Formatter(LOG_FORMAT))
disk_logger.addHandler(disk_file_handler)