import json
from logger import network_logger

from System_Monitor.src.system_monitor.Model import KernelInterface
from System_Monitor.src.system_monitor.Model import KrnlInterfaces
from System_Monitor.src.system_monitor.Model import ActiveInternetConnections
from System_Monitor.src.system_monitor.Model import Network
from System_Monitor.src.system_monitor.Model import TCPConnection
from System_Monitor.src.system_monitor.Model import UDPConnection

temp_list = []


def sshConnectNetwork(host, username, password, json_Path):
    try:

        import paramiko

        # Connect to remote host
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(host, username=username, password=password)

        stdin, stdout, stderr = client.exec_command('netstat -i')
        stdin1, stdout1, stderr1 = client.exec_command('netstat -pt')
        stdin2, stdout2, stderr2 = client.exec_command('ifconfig')
        stdin3, stdout3, stderr3 = client.exec_command('netstat -pu')

        kernel_interface = stdout.readlines()
        tcp_connections = stdout1.readlines()
        if_info = stdout2.readlines()
        udp_connections = stdout3.readlines()
        host = str(host) + "_networkLogs"

        network_info = []
        for lines in kernel_interface:
            network_info.append(lines)
        for lines in tcp_connections:
            network_info.append(lines)
        for lines in udp_connections:
            network_info.append(lines)
        for lines in if_info:
            network_info.append(lines)
        for lines in network_info:
            network_logger.info(lines)

        network_Info(kernel_interface, tcp_connections, if_info, udp_connections, json_Path)

        stdin.close()
        stdout.close()
        stderr.close()
        client.close()
    except IndexError:
        pass


# Returns a list of kernel network information of system
def kernel_network_info(krnl_intrface):
    kernel_list = []
    kernel_count = 0

    for item in krnl_intrface:

        kernel_count += 1

        if kernel_count == 0 or kernel_count == 1 or kernel_count == 2:

            continue

        else:

            kernel_interface = KrnlInterfaces.Interface()

            kernel_interface.set_iface = item.split()[0]
            kernel_interface.set_mtu = item.split()[1]
            kernel_interface.set_rx_ok = item.split()[2]
            kernel_interface.set_rx_err = item.split()[3]
            kernel_interface.set_rx_drp = item.split()[4]
            kernel_interface.set_rx_ovr = item.split()[5]
            kernel_interface.set_tx_ok = item.split()[6]
            kernel_interface.set_tx_err = item.split()[7]
            kernel_interface.set_tx_drp = item.split()[8]
            kernel_interface.set_tx_ovr = item.split()[9]
            kernel_interface.set_flag = item.split()[10]

            kernel_list.append(kernel_interface)

    return kernel_list


# Returns a list of tcp connections in system
def tcp_connection_info(tcp_connections):
    tcp_count = 0
    tcp_list = []

    for item in tcp_connections:

        tcp_count += 1

        if tcp_count == 0 or tcp_count == 1 or tcp_count == 2:

            continue

        else:

            tcp_connection = TCPConnection.TCP()

            local_address = item.split()[3]

            temp = local_address[0:12]

            tcp_connection.set_protocol = item.split()[0]
            tcp_connection.set_recv_q = item.split()[1]
            tcp_connection.set_send_q = item.split()[2]
            tcp_connection.set_local_address = temp
            tcp_connection.set_foreign_address = item.split()[4]
            tcp_connection.set_state = item.split()[5]
            tcp_connection.set_program_name = item.split()[6]

            tcp_list.append(tcp_connection)

    return tcp_list


# Returns a list of udp connection in system
def udp_connection_info(udp_connections):
    udp_count = 0
    udp_list = []

    for item in udp_connections:

        udp_count += 1

        if udp_count == 0 or udp_count == 1 or udp_count == 2:

            continue

        else:

            udp_connection = UDPConnection.UDP()

            local_address = item.split()[3]

            temp = local_address[0:12]

            udp_connection.set_protocol = item.split()[0]
            udp_connection.set_recv_q = item.split()[1]
            udp_connection.set_send_q = item.split()[2]
            udp_connection.set_local_address = temp
            udp_connection.set_foreign_address = item.split()[4]
            udp_connection.set_state = item.split()[5]
            udp_connection.set_program_name = item.split()[6]

            udp_list.append(udp_connection)

    return udp_list


def fetch_ip_address(ip_info):
    temp = []
    ip_list = []

    for line in ip_info:

        if 'inet' in line.split():
            temp.append(line)

    for line in temp:
        ip_string = line.split()[1]
        temp_string = ip_string[5:]
        ip_list.append(temp_string)

    return ip_list


def merge_ip_kernel(kernel_list, ip_list):
    for index in range(len(kernel_list)):
        kernel_list[index].set_ip_address = ip_list[index]

    return kernel_list


def network_Info(krnl_intrface, tcp_connections, ip_info, udp_connections, json_Path):
    network = Network.Network()
    active_internet_connection = ActiveInternetConnections.ActiveConnection()
    kernel = KernelInterface.Kernel()

    active_internet_connection.set_tcp_connection = tcp_connection_info(tcp_connections)
    active_internet_connection.set_udp_connection = udp_connection_info(udp_connections)

    kernel_list = merge_ip_kernel(kernel_network_info(krnl_intrface), fetch_ip_address(ip_info))

    kernel.set_kernel_interface = kernel_list

    network.set_kernel_interface = kernel
    network.set_active_internet_connection = active_internet_connection

    network_json = json.dumps(network, default=lambda x: x.__dict__, sort_keys=False, indent=4)
    file = open(json_Path + "network.json", "w")
    file.write(network_json)
    file.close()
    # print(network_json)




