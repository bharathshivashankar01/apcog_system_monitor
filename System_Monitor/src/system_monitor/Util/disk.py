import json
from logger import disk_logger
from System_Monitor.src.system_monitor.Model import Disk, DiskPartition, FileSystemPartition, Partition


def sshConnectDisk(host, username, password, json_Path):
    try:

        import paramiko

        # Connect to remote host
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(host, username=username, password=password)

        stdin, stdout, stderr = client.exec_command('lsblk')
        stdin1, stdout1, stderr1 = client.exec_command('df -h')

        disk = stdout.readlines()
        file_system = stdout1.readlines()
        disk_info = []
        for lines in disk:
            disk_info.append(lines)
        for lines in file_system:
            disk_info.append(lines)

        for lines in disk_info:
            disk_logger.info(lines)

        disk_Info(disk, file_system, json_Path)

        stdin.close()
        stdout.close()
        stderr.close()
        client.close()

    except IndexError:

        pass


def disk_information(dsk_prtitn):
    disk_count = 0
    disk_partition_list = []

    for line in dsk_prtitn:

        disk_count += 1

        if disk_count == 0 or disk_count == 1:

            continue

        else:

            d_partition = DiskPartition.Partition()

            name = line.split()[0]

            if len(name) >= 3:
                name = name[-4:]

            d_partition.set_name = name
            d_partition.set_rm = line.split()[2]
            d_partition.set_size = line.split()[3]
            d_partition.set_type = line.split()[4]
            d_partition.set_mount_point = line.split()[5]

            disk_partition_list.append(d_partition)

    return disk_partition_list


def file_sys_info(file_sys_prtitn):
    file_count = 0
    file_partition_list = []

    for line in file_sys_prtitn:

        file_count += 1

        print(line)

        if file_count == 0 or file_count == 1:

            continue

        else:

            f_partition = FileSystemPartition.Partition()

            f_partition.set_name = line.split()[0]
            f_partition.set_size = line.split()[1]
            f_partition.set_used = line.split()[2]
            f_partition.set_available = line.split()[3]
            f_partition.set_use_percentage = line.split()[4]
            f_partition.set_mount_on = line.split()[5]

            file_partition_list.append(f_partition)

    return file_partition_list


def disk_Info(dsk_prtitn, file_sys_prtitn, json_Path):
    disk = Disk.Disk()
    partition = Partition.Partitions()

    partition.set_disk_partition = disk_information(dsk_prtitn)
    partition.set_file_system_partition = file_sys_info(file_sys_prtitn)

    disk.set_partition = partition

    disk_json = json.dumps(disk, default=lambda x: x.__dict__, sort_keys=False, indent=4)
    file = open(json_Path + "disk_info.json", "w")
    file.write(disk_json)
    file.close()

    # print(disk_json)



