# Author    : Harshith S R
# Date      : 23/08/2018

import json
from logger import memory_logger

from System_Monitor.src.system_monitor.Model import Memory
from System_Monitor.src.system_monitor.Model import SwapMemory
from System_Monitor.src.system_monitor.Model import VirtualMemory

# print("\nHost name : ", socket.gethostname())

memory = Memory.Memory()
virtual_memory = VirtualMemory.Virtual()
swap_memory = SwapMemory.Swap()
host_name = 'N'


# this method fetches the memory usage of a system
# all the memory fetched is in KB's
def fetch_virtual_memory(output):
    # with open('/proc/meminfo', 'r') as memory_file:
    memory = output

    # index 0 : total memory
    virtual_memory.set_total = int(memory[0].split()[1])

    # index 1 : free memory
    virtual_memory.set_free = int(memory[1].split()[1])

    # (index 0 - index 1) to calculate the used memory
    virtual_memory.set_used = (int(memory[0].split()[1]) - int(memory[1].split()[1]))

    # index 2 : available memory
    virtual_memory.set_available = int(memory[2].split()[1])

    # index 3 : buffer memory
    virtual_memory.set_buffer = int(memory[3].split()[1])

    # index 4 : cached memory
    virtual_memory.set_cached = int(memory[4].split()[1])

    # index 6 : active memory
    virtual_memory.set_active = int(memory[6].split()[1])

    # index 7 : inactive memory
    virtual_memory.set_inactive = int(memory[7].split()[1])

    # index 21 : slab memory
    virtual_memory.set_slab = int(memory[21].split()[1])

    '''
    # below line converts the python object to JSON object
    # sort_keys : if True will sort keys in the ascending order
    # indent : indent spaces for JSON object and member
    virtual_memory_json = json.dumps(virtual_memory.__dict__, sort_keys=False, indent=4)

    print("\n **************************Virtual Memory Details********************************* : \n")
    print(virtual_memory_json)
    
    '''


def fetch_swap_memory(output):
    # with open('/proc/meminfo', 'r') as memory_file:
    memory = output

    # index 14 : total swap memory
    swap_memory.set_total = int(memory[14].split()[1])

    # index 15 : free swap memory
    swap_memory.set_free = int(memory[15].split()[1])

    # (index 14 - index 15) to calculate the used swap memory
    swap_memory.set_used = (int(memory[14].split()[1]) - int(memory[15].split()[1]))

    '''
    # below line converts the python object to JSON object
    # sort_keys : if True will sort keys in the ascending order
    # indent : indent spaces for JSON object and member
    swap_memory_json = json.dumps(swap_memory.__dict__, sort_keys=False, indent=4)

    print("\n **************************Swap Memory Details********************************* : \n")
    print(swap_memory_json)
    '''


def sshConnectMemory(host, username, password, json_Path):
    try:

        import paramiko

        # Connect to remote host
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(host, username=username, password=password)

        stdin, stdout, stderr = client.exec_command('cat /proc/meminfo')
        output = stdout.readlines()
        host = str(host) + "_memoryLogs"
        for lines in output:
            memory_logger.info(lines)

        main(output, json_Path)

        stdin.close()
        stdout.close()
        stderr.close()
        client.close()
    except IndexError:
        pass


def main(output, json_Path):
    fetch_virtual_memory(output)
    fetch_swap_memory(output)

    memory.set_virtual = virtual_memory
    memory.set_swap = swap_memory

    memory_json = json.dumps(memory, default=lambda x: x.__dict__, sort_keys=False, indent=4)
    file = open(json_Path + "memory.json", "w")
    file.write(memory_json)
    file.close()
    print(memory_json)



