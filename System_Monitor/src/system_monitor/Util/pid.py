import re
import json
from logger import pid_logger

from System_Monitor.src.system_monitor.Model import ProcessInfo, Processes


def assign_pid_values(proc_info):
    process_info = ProcessInfo.Information()
    process_info.set_user = proc_info[0]
    process_info.set_pid = proc_info[1]
    process_info.set_cpu_percentage = proc_info[2]
    process_info.set_memory_percentage = proc_info[3]
    process_info.set_time = proc_info[9]
    process_info.set_command = proc_info[10]

    return process_info


def sshConnectPid(host, username, password, json_Path):
    try:

        import paramiko

        # Connect to remote host
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(host, username=username, password=password)

        stdin, stdout, stderr = client.exec_command('ps aux')
        output = stdout.readlines()
        host = str(host) + "_pidLogs"
        for lines in output:
            pid_logger.info(lines)

        fetch_pid(output, json_Path)

        stdin.close()
        stdout.close()
        stderr.close()
        client.close()

    except IndexError:
        pass


def fetch_pid(output, json_Path):
    proc_list = []
    processes = Processes.Process()

    for line in output:
        # The separator for splitting is 'variable number of spaces'
        proc_info = re.split(" +", line.strip())
        proc_list.append(assign_pid_values(proc_info))
    print('Process list:n\n')

    processes.set_pid_list = proc_list
    pid_json = json.dumps(proc_list, default=lambda x: x.__dict__, sort_keys=False, indent=4)
    # print(pid_json)
    file = open(json_Path + "pid.json", "w")
    file.write(pid_json)
    file.close()
