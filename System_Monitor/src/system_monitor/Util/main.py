import schedule
import pid
import time
import memory
import getpass
import sensors
import network
import cpu_info
import disk
import SystemInfo


def job():
    # print("I'm working...")
    memory.sshConnectMemory(host, username, password, json_Path)
    pid.sshConnectPid(host, username, password, json_Path)
    sensors.sshConnectSensors(host, username, password, json_Path)
    network.sshConnectNetwork(host, username, password, json_Path)
    cpu_info.sshConnectCPU_Info(host, username, password, json_Path)
    disk.sshConnectDisk(host, username, password, json_Path)
    SystemInfo.sshConnectSysInfo(host, username, password, json_Path)


if __name__ == "__main__":
    host = input("enter the host name or IP address\n")
    username = input("enter the username\n")
    # password = input("enter the password\n")
    password = getpass.getpass(prompt='Password: ', stream=None)
    json_Path = "/home/apcog_system_monitor/System_Monitor/Views/"
    schedule.every(3).seconds.do(job)
    while True:
        schedule.run_pending()
        time.sleep(1)
    # job()

# def kill_processes(pid):
#     os.kill(pid, signal.SIGKILL)
