import json
from logger import sensors_logger
from System_Monitor.src.system_monitor.Model import Sensors


def sshConnectSensors(host, username, password, json_Path):
    try:

        import paramiko

        # Connect to remote host
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(host, username=username, password=password)

        stdin, stdout, stderr = client.exec_command('cat /sys/class/thermal/thermal_zone0/temp')
        output = stdout.readlines()[0]
        host = str(host) + "_sensorLogs"
        for lines in output:
            sensors_logger.info(lines)
        sensors(output, json_Path)

        stdin.close()
        stdout.close()
        stderr.close()
        client.close()
    except IndexError:
        pass


def sensors(output, json_Path):
    sensor = Sensors.Sensor()
    sensor.set_temperature = (int(output) / 1000)
    cpu_json = json.dumps(sensor, default=lambda x: x.__dict__, sort_keys=False, indent=4)
    file = open(json_Path + "temperature.json", "w")
    file.write(cpu_json)
    file.close()
    print(cpu_json)

